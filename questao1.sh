#!/bin/bash

for i in $(ls /etc /tmp); do
   [-d $I] && echo $I é um diretório && continue
   [-f $I] && echo $I é um arquivo
   [-x $I] && echo $I é um executável
   [-L $I] && echo $I é um link!

done 
